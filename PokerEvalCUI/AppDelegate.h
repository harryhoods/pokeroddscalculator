//
//  AppDelegate.h
//  PokerEvalCUI
//
//  Created by Hari Prabhakaran on 3/3/13.
//  Copyright (c) 2013 Hari Prabhakaran & Chang Luo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
