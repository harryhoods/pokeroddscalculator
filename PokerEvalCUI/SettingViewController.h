//
//  SettingViewController.h
//  ScoopOdds
//
//  Created by Chang Luo on 6/17/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, SOGame){
    SOGameHoldem,
    SOGameOmaha,
    SOGameOmaha8,
    SOGameCount
};

@protocol SettingDelegate <NSObject>
-(void)didSelectPlayers:(NSUInteger)players;
-(void)didSelectGame:(SOGame)game;
-(void)didExitSetting;
@end

@interface SettingViewController : UITableViewController
@property (nonatomic) id<SettingDelegate> delegate;
@end
