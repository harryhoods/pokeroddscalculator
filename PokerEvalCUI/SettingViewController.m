//
//  SettingViewController.m
//  ScoopOdds
//
//  Created by Chang Luo on 6/17/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

#import "SettingViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface SettingViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation SettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectNull];
    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.textAlignment = NSTextAlignmentCenter;
    sectionHeader.font = [UIFont systemFontOfSize:12];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, 30);
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *locinfo = [bundle localizedInfoDictionary];
    NSString *productName =  [locinfo objectForKey:@"CFBundleDisplayName"];
    sectionHeader.text = [productName stringByAppendingString:version];

    self.tableView.tableFooterView = sectionHeader;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) sendEmail :(NSString*) subject :(NSArray*) recipients {
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    
    if(recipients != nil)
        [controller setToRecipients:recipients];
    
    [controller setSubject:subject];
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void) emailSupport {
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    NSBundle *bundle = [NSBundle mainBundle];
    NSDictionary *locinfo = [bundle localizedInfoDictionary];
    NSString *productName =  [locinfo objectForKey:@"CFBundleDisplayName"];
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString *device = [[UIDevice currentDevice] model];
    NSString *subject = [NSString stringWithFormat:@"%@ v%@ (%@ %@)",
                         productName,
                         version,
                         device,
                         systemVersion];
    
    NSArray* recipients = @[@"info@scoopodds.com"];
    
    [self sendEmail:subject :recipients];
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *) error{
    [self becomeFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) tweetSupport {
    if(([[[UIDevice currentDevice] systemVersion] doubleValue] < 6.0)) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"This feature requires %@ version %@ or later.  Yours is %@.", @"%1$@ is either iOS or Android.  %2$@ and %3$@ are the version number e.g. 6.0."), @"iOS", @"6.0", [[UIDevice currentDevice] systemVersion]] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
        
        [alert show];
        return;
    }
    
    SLComposeViewController* controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    [controller setInitialText:@".@ScoopOdds "];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Player Segment

-(void)selectNoOfPlayers:(id)sender{
    UISegmentedControl *segment = sender;
    if (self.delegate){
        [self.delegate didSelectPlayers:segment.selectedSegmentIndex + 2];
        [self.delegate didExitSetting];
    }
}

-(void)renewPlayersSegment:(UITableViewCell *) cell{
    UISegmentedControl *playersSegment = nil;
    // Support 4 players for all games for simplicity
//    if(game==game_holdem){
//        playersSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"2",@"3",@"4",@"5",@"6",nil]];
//        //Disabling the player 5/6 selection - temporarily, until real-estate for player 5/6 is fixed on the felt.
//        [playersSegment setEnabled:NO forSegmentAtIndex:3];
//        [playersSegment setEnabled:NO forSegmentAtIndex:4];
//    }
//    else
    playersSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"2",@"3",@"4",nil]];
    
    playersSegment.segmentedControlStyle=UISegmentedControlStylePlain;
    playersSegment.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    playersSegment.frame = cell.contentView.bounds;
    
    playersSegment.selectedSegmentIndex=2;
    
    [playersSegment addTarget:self action:@selector(selectNoOfPlayers:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:playersSegment];
}

#pragma mark - Table view data source
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

/*
 - (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
 
 if(section == 1)
 return @"Players";//@"Game Settings";
 else
 return nil;//@"Other";
 }
 */

// built-in headers' shadow don't look good
//- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    switch (section) {
//        case 1:
//            return NSLocalizedString(@"Players", @"Number of players");
//            break;
//        default:
//            break;
//    }
//    return nil;
//}
#pragma mark - Table view delegate
NS_ENUM(NSUInteger, SettingSection){
    SettingSectionPlayers,
    SettingSectionGame,
    SettingSectionContact,
    SettingSectionCount
};
NS_ENUM(NSUInteger, SettingContact){
    SettingContactTwitter,
    SettingContactEmail,
    SettingContactHelp,
    SettingContactMoreApps,
    SettingContactCount
};

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectNull];
    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.textAlignment = NSTextAlignmentCenter;
    sectionHeader.font = [UIFont systemFontOfSize:16];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.frame = CGRectMake(0, 0, tableView.bounds.size.width, 30);
    switch (section) {
        case SettingSectionGame:
            // Remove section header in order to save space.
            // The game names themselves are self-explanatory enough.
            break;
        case SettingSectionPlayers:
            sectionHeader.text = NSLocalizedString(@"Players", @"Number of players");
            break;
        case SettingSectionContact:{
        }
        default:
            break;
    }
    return sectionHeader;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch(section){
        case SettingSectionPlayers: return 30;
        default: break;
    }
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch(section){
        case SettingSectionGame: return SOGameCount;
        case SettingSectionPlayers: return 1;
        case SettingSectionContact: return SettingContactCount;
        default: return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
    
    cell.textLabel.font = [UIFont systemFontOfSize:17];
    [cell.textLabel setTextColor:[UIColor darkGrayColor]];
    
    switch (indexPath.section) {
        case SettingSectionGame:
            switch (indexPath.row) {
                case SOGameHoldem:{
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@",@"Holdem"), NSLocalizedString(@"Holdem", @"Acronym for Texas Hodlem")];
                    UITableViewCell *selectedCell=nil;
                    for(int i=0;i<SOGameCount;i++){
                        selectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
                        if(selectedCell.accessoryType==UITableViewCellAccessoryCheckmark)
                            selectedCell.accessoryType=UITableViewCellAccessoryNone;
                    }
                    cell.accessoryType=UITableViewCellAccessoryCheckmark;
                    
                    break;
                }
                case SOGameOmaha:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Omaha"), NSLocalizedString(@"Omaha", nil)];
                    break;
                case SOGameOmaha8:
                    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@", @"Omaha Hi-Lo"), NSLocalizedString(@"Omaha Hi-Lo", nil)];
                    break;
                default:
                    break;
            }
            break;
        case SettingSectionPlayers:
            [self renewPlayersSegment:cell];
            break;
        case SettingSectionContact:
            switch (indexPath.row) {
                case SettingContactTwitter:
                    cell.textLabel.text = @"Twitter @ScoopOdds";
                    break;
                case SettingContactEmail:
                    cell.textLabel.text = NSLocalizedString(@"Email Support", nil);
                    break;
                case SettingContactHelp:
                    cell = [tableView dequeueReusableCellWithIdentifier:@"FAQCell"];
                    cell.textLabel.font = [UIFont systemFontOfSize:17];
                    [cell.textLabel setTextColor:[UIColor darkGrayColor]];
                    cell.textLabel.text = NSLocalizedString(@"FAQ", nil);
                    break;
                case SettingContactMoreApps:
                    cell.textLabel.text = NSLocalizedString(@"More Apps", nil);
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case SettingSectionGame:
            if (self.delegate)
                [self.delegate didSelectGame:indexPath.row];
            break;
        case SettingSectionPlayers:
            break;
        case SettingSectionContact:
            switch (indexPath.row) {
                case SettingContactTwitter:
                    [self tweetSupport];
                    break;
                case SettingContactEmail:
                    [self emailSupport];
                    break;
                case SettingContactHelp:
                    break;
                case SettingContactMoreApps:{
                    NSString *iTunesLink = @"https://itunes.apple.com/us/artist/poker-chang-ltd/id312276632";
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
                    break;
                }
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
    UITableViewCell *selectedCell = nil;
    if(indexPath.section==0){
        for(int i=0;i<SOGameCount;i++){
            selectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            if(selectedCell.accessoryType==UITableViewCellAccessoryCheckmark)
                selectedCell.accessoryType=UITableViewCellAccessoryNone;
        }
        selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [self renewPlayersSegment:[tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]]];
        //when gameType in section "0" changes, change Players Option in section "1"
    }
    
    if(self.delegate)
        [self.delegate didExitSetting];
    
    //gameType.highlightedTextColor = [UIColor whiteColor];
    //gameType.highlighted = YES;
    
    //Simulate a touch event on a button
    //[self.buttonClear sendActionsForControlEvents: UIControlEventTouchUpInside];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
-(void) faq{
    [self performSegueWithIdentifier:@"FAQ" sender:self];
}

@end
