//
//  ViewController.h
//  PokerEvalCUI
//
//  Created by Hari Prabhakaran on 3/3/13.
//  Copyright (c) 2013 Hari Prabhakaran & Chang Luo. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <time.h>

#import <QuartzCore/QuartzCore.h> //for Uibutton CALayer edit

#include "include/enumdefs.h"
#include "include/poker_defs.h"
#include "inlines/eval.h"

#import <UIKit/UIKit.h>
#import "CardFromDeck.h"

#define MAX_PLAYERS 6
#define MAX_CARDS_PER_PLAYER 7

@interface ViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UIView *backgroundView;
    BOOL isMenuShown;
    
    int newCardX;
    int newCardY;
    int totalSelectedCards;
    int totalOpenCards;
    int boardCards[5];
    int boardcount;
    int unSelectedCount;
    int unSelected[16][2];
    bool isiPhone5;
    //float screenBrightness;
    
    NSMutableArray *cardImages;
    
    //UIImageView *cardImage;
    UIImageView *addCard;
    UILabel *addStat[2],*statLabel;
    UITapGestureRecognizer *singleFingerTap;
    UIActivityIndicatorView *spinner;
    UILabel *feltLabel;  //dynamic display on felt

    UISwipeGestureRecognizer *swipeLeft;
    UISwipeGestureRecognizer *swipeRight;
    
    //Players Variable, 6 players max with 
    int playerCards[MAX_PLAYERS][MAX_CARDS_PER_PLAYER];
    unsigned short player1StartXPos;
    unsigned short player2StartXPos;
    NSString *gameTypeStr;
    unsigned short playersPerGame;
    unsigned short cardsPerPlayer;
    unsigned short boardStartX;
    enum_game_t game;
    enum_ordering_mode_t orderMode;
    
    unsigned int iterationCount;
    unsigned short bringFrontX;
    unsigned short bringFrontY;
}

@property (strong, nonatomic) IBOutlet UIImageView *felt;
@property (strong, nonatomic) IBOutlet UIButton *buttonClear;
@property (strong, nonatomic) IBOutlet UIButton *buttonCalc;
@property (strong, nonatomic) IBOutlet UIButton *buttonMenu;

- (IBAction)reset:(id) sender;
- (IBAction)revealMenu:(id) sender;
- (IBAction)calcOdds:(id)sender;

@property (strong, nonatomic) IBOutlet UICollectionView *deckCollection;

-(void) selectCards:(int) index;
-(void) addOdds;
-(void) handleSingleTap:(UITapGestureRecognizer *)recognizer;
-(void) setCardPos;
-(void) addSubViewImage:(int) x :(int) y :(NSString*) imageName :(int) index;
-(void) bringNextCardToFront:(int) x :(int) y;

@end
