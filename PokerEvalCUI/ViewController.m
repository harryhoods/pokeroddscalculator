//
//  ViewController.m
//  PokerEvalCUI
//
//  Created by Hari Prabhakaran on 3/3/13.
//  Copyright (c) 2013 Hari Prabhakaran & Chang Luo. All rights reserved.
//

#import "ViewController.h"
#import "SettingViewController.h"

@interface ViewController () <SettingDelegate>
@end

@implementation ViewController

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"SettingViewController"]){
        SettingViewController *settingViewController = segue.destinationViewController;
        settingViewController.delegate = self;
    }
}
#pragma mark - Setting Delegate

-(void)didSelectGame:(SOGame)sogame{
    switch (sogame) {
        case SOGameHoldem:
            [self selectedHoldem];
            break;
        case SOGameOmaha:
            [self selectedOmaha];
            break;
        case SOGameOmaha8:
            [self selectedOmaha8];
            break;
        default:
            break;
    }
    feltLabel.text=gameTypeStr;
}

-(void)didSelectPlayers:(NSUInteger)players{
    int oldPlayersPerGame=playersPerGame;
    playersPerGame=players;
    
    //Clear stats and iteration count
    [self clearStats];
    feltLabel.font=[UIFont systemFontOfSize:17];
    feltLabel.text=gameTypeStr;
    
    //Clear Player hands only of new selection is less than previous
    if(playersPerGame<oldPlayersPerGame){
        if(oldPlayersPerGame>2)
            [self clearBoard:YES andPlayer3:YES currentPlayerCount:playersPerGame];
    }
    else{
        [self clearBoard:YES andPlayer3:NO currentPlayerCount:oldPlayersPerGame];
    }

}
-(void)didExitSetting{
    [self hideMenu];
}
#pragma mark - Setup
- (void)initSetup {
    unsigned short i,j; // iterator;
    totalSelectedCards=0;
    totalOpenCards=0;
    boardcount=0;
    unSelectedCount=0;
    iterationCount=0;
    for(i=0;i<MAX_PLAYERS;i++){
        for(j=0;j<MAX_CARDS_PER_PLAYER;j++)
            playerCards[i][j]=-1;
        if(i<5)
            boardCards[i]=-1;
    }
    newCardX=player1StartXPos;
    newCardY=60;
    if(isiPhone5)
        newCardY+=40;    
    
    feltLabel.font=[UIFont systemFontOfSize:17];
    feltLabel.text=gameTypeStr;
    boardStartX=45;
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    spinner.center = CGPointMake(160, 240);
    spinner.hidesWhenStopped = YES;
    spinner.hidden=YES;
    self.buttonCalc.alpha=0.6;
    self.buttonCalc.userInteractionEnabled=NO;
}


-(void) selectedHoldem{
    gameTypeStr=NSLocalizedString(@"Holdem",nil);
    cardsPerPlayer=2;
    player1StartXPos=40;
    player2StartXPos=210;
    game=game_holdem;
    orderMode=enum_ordering_mode_hi;
    [self reset:nil];
}

-(void) selectedOmaha{
    gameTypeStr=NSLocalizedString(@"Omaha",nil);
    cardsPerPlayer=4;
    player1StartXPos=20;
    player2StartXPos=190;
    game=game_omaha;
    orderMode=enum_ordering_mode_hi;
    [self reset:nil];
}

-(void) selectedOmaha8{
    [self selectedOmaha];
    gameTypeStr=NSLocalizedString(@"Omaha Hi-Lo",nil);
    game=game_omaha8;
    orderMode=enum_ordering_mode_hilo;
}

-(void) selectedStud{
    gameTypeStr=NSLocalizedString(@"7card Stud",nil);
    cardsPerPlayer=7;
    player1StartXPos=0;
    player2StartXPos=190;
    game=game_7stud;
    orderMode=enum_ordering_mode_hi;
    [self reset:nil];
}

-(void) selectedStud8{
    [self selectedStud];
    gameTypeStr=NSLocalizedString(@"7card Stud Hi-Lo",nil);
    game=game_7stud8;
    orderMode=enum_ordering_mode_hilo;
}

-(void) selectedRazz{
    [self selectedStud];
    gameTypeStr=NSLocalizedString(@"7card Stud Hi-Lo",nil);
    game=game_razz;
    orderMode=enum_ordering_mode_lo;
}

//typedef enum{
//    Holdem,
//    Omaha,
//    Omaha8,
//    section0Count, //section seperator
//    noOfPlayers,
//    TweetSupport,
//    EmailSupport,
//    FAQ,
//    SettingCount
//}SettingCell;

#pragma mark - View
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
        /* BUTTON STYLING
         [[self.buttonMenu layer] setBorderWidth:1.0f];
         [[self.buttonMenu layer] setBorderColor:[UIColor whiteColor].CGColor];
    
         [[self.buttonCalc layer] setBorderWidth:1.0f];
         [[self.buttonCalc layer] setBorderColor:[UIColor whiteColor].CGColor];
    
         [[self.buttonClear layer] setBorderWidth:1.0f];
         [[self.buttonClear layer] setBorderColor:[UIColor whiteColor].CGColor];
         */
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    //screenBrightness=[UIScreen mainScreen].brightness;
    playersPerGame=2;
    
    //Iphone 5 - change the felt; x:y posision of cards displayed
    isiPhone5=false;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    if (screenRect.size.height >= 568.0f){
        [self.felt setImage:[UIImage imageNamed:@"Felt.640.1136.png"]];
        isiPhone5=true;
        //gameType.frame = CGRectOffset(gameType.frame,0,CGRectGetMidY(gameType.frame)-100);
    }
    
    [self selectedHoldem];
    
    unsigned short labelY=250;
    if(isiPhone5)
        labelY+=40;
    
    CGRect gameLabelFrame=CGRectMake(93,labelY,134,19);
    feltLabel=[[UILabel alloc] initWithFrame:gameLabelFrame];
    feltLabel.tag=-1; //-1 are primary subviews that will not be removed dynamically.
    feltLabel.backgroundColor=[UIColor clearColor];
    feltLabel.font=[UIFont systemFontOfSize:17];
    feltLabel.textColor=[UIColor darkGrayColor];
    feltLabel.textAlignment=NSTextAlignmentCenter;
    feltLabel.text=gameTypeStr;
    [backgroundView addSubview:feltLabel];
    
    gameLabelFrame=CGRectMake(screenRect.size.width/2-15,labelY+18,30,30);
    self.buttonCalc=[[UIButton alloc]initWithFrame:gameLabelFrame];
    self.buttonCalc.tag=-1;
    self.buttonCalc.showsTouchWhenHighlighted=YES;
    self.buttonCalc.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    self.buttonCalc.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [self.buttonCalc setSelected:YES];
    [self.buttonCalc setImage:[UIImage imageNamed:@"redoButn.png"] forState:UIControlStateSelected];
    [self.buttonCalc addTarget:self action:@selector(calcOdds:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:self.buttonCalc];
    
    [self initSetup]; //selectedHoldem Automatically calls reset, which calls initSetup
    
    swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [backgroundView addGestureRecognizer:swipeLeft];
    
    swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [backgroundView addGestureRecognizer:swipeRight];
    
    //[self.deckCollection registerClass:[UICollectionViewCell Class] forCellWithReuseIdentifier:@"Cell"];

    [self.deckCollection setDataSource:self];
    [self.deckCollection setDelegate:self];

    cardImages = [[NSMutableArray alloc]initWithObjects:
@"0.png",@"1.png",@"2.png",@"3.png",@"4.png",@"5.png",@"6.png",@"7.png",@"8.png",@"9.png",@"10.png",@"11.png",@"12.png",
@"13.png",@"14.png",@"15.png",@"16.png",@"17.png",@"18.png",@"19.png",@"20.png",@"21.png",@"22.png",@"23.png",@"24.png",@"25.png",
@"26.png",@"27.png",@"28.png",@"29.png",@"30.png",@"31.png",@"32.png",@"33.png",@"34.png",@"35.png",@"36.png",@"37.png",@"38.png",
@"39.png",@"40.png",@"41.png",@"42.png",@"43.png",@"44.png",@"45.png",@"46.png",@"47.png",@"48.png",@"49.png",@"50.png",@"51.png",nil];
}

//Data source and delegate methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
    //return (int)(cardImages.count/13);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [cardImages count];
    //return 13;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cardIdentifier = @"card";
    CardFromDeck *card = [collectionView dequeueReusableCellWithReuseIdentifier:cardIdentifier forIndexPath:indexPath];
    [[card cardInCell]setImage:[UIImage imageNamed:[cardImages objectAtIndex:indexPath.item]]];
    if(card.cardInCell.image == [UIImage imageNamed:@"back.png"])
        card.userInteractionEnabled=NO;
    else
        card.userInteractionEnabled=YES;
    card.tag=-1;
    return card;
};

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//NSLog(@"didSelectItemAtIndexPath: objectAtIndex:%d",indexPath.item);
    NSString *imageName;
    imageName = [NSString stringWithFormat:@"%d.png" ,indexPath.item];
 
    CardFromDeck *card = (CardFromDeck *)[collectionView cellForItemAtIndexPath:indexPath];
    if(card.cardInCell.image != [UIImage imageNamed:@"back.png"]){
        @synchronized(cardImages){
            [cardImages replaceObjectAtIndex:indexPath.item withObject:@"back.png"];
        }
        [card.cardInCell setImage:[UIImage imageNamed:@"back.png"]];
        card.userInteractionEnabled=NO;
    }
    else
        [card.cardInCell setImage:[UIImage imageNamed:imageName]];

    [self selectCards:indexPath.item];
}

-(void) selectCards:(int) index{
    NSString *imageName;
    imageName = [NSString stringWithFormat:@"%d.png",index];
    bool calcOddsFlag=true;
    unsigned short i=0,j=0; // iterator;
    if(unSelectedCount==0){
        [self setCardPos];
        [self addSubViewImage:newCardX :newCardY :imageName :index];
        //COMMON card mask - the community.. It cannot go beyond 5.. so locking it at 5.
        boardcount=(boardcount<5)?boardcount:5;
        if(boardcount==5){
            NSLog(@"Why is the control entering here? IF boardCount is full and no selected, disable DECK");
            [self reset:nil];
        }
        else if(totalSelectedCards>=(playersPerGame*cardsPerPlayer))
            boardCards[boardcount++]=index;
        else{
            playerCards[totalSelectedCards/cardsPerPlayer][totalSelectedCards%cardsPerPlayer]=index;
            //NSLog(@"playerCards[%d][%d]:%d",totalSelectedCards/cardsPerPlayer,totalSelectedCards%cardsPerPlayer,index);    //DEBUG
        }
        totalSelectedCards++;
        
    }
    else{
        /* REverse FILL.. like a Stack.. LIFO */
        [self addSubViewImage:unSelected[--unSelectedCount][0] :unSelected[unSelectedCount][1] :imageName :index];
        //NSLog(@"unSelected[%d][0]:%d",unSelectedCount,unSelected[unSelectedCount][0]);
        
        if(unSelected[unSelectedCount][1]>190 && unSelected[unSelectedCount][1]<250){
            if(unSelected[unSelectedCount][0]==boardStartX) //Board
                boardCards[0]=index;
            else if(unSelected[unSelectedCount][0]==boardStartX+45)
                boardCards[1]=index;
            else if(unSelected[unSelectedCount][0]==boardStartX+90)
                boardCards[2]=index;
            else if(unSelected[unSelectedCount][0]==boardStartX+135)
                boardCards[3]=index;
            else if(unSelected[unSelectedCount][0]==boardStartX+180)
                boardCards[4]=index;
        }
        else if(unSelected[unSelectedCount][1]<100)
            i=0;
        else if(unSelected[unSelectedCount][1]>=330 && unSelected[unSelectedCount][1]<=420)
            i=2;
        {
            //NSLog(@"i:%d Index:%d x:%d y:%d",i,index,unSelected[unSelectedCount][0],unSelected[unSelectedCount][1]);
            if(unSelected[unSelectedCount][0]==player1StartXPos)
                playerCards[i][0]=index;
            else if(unSelected[unSelectedCount][0]==player1StartXPos+22)
                playerCards[i][1]=index;
            else if(unSelected[unSelectedCount][0]==player1StartXPos+44)
                playerCards[i][2]=index;
            else if(unSelected[unSelectedCount][0]==player1StartXPos+66)
                playerCards[i][3]=index;
            else if(unSelected[unSelectedCount][0]==player2StartXPos)
                playerCards[i+1][0]=index;
            else if(unSelected[unSelectedCount][0]==player2StartXPos+22)
                playerCards[i+1][1]=index;
            else if(unSelected[unSelectedCount][0]==player2StartXPos+44)
                playerCards[i+1][2]=index;
            else if(unSelected[unSelectedCount][0]==player2StartXPos+66)
                playerCards[i+1][3]=index;
        }

    }
    
    //IF all the cards are not present; dont automatically calc-odds.
    for(j=0;j<playersPerGame;j++){
        for(i=0;i<cardsPerPlayer;i++){
            if(playerCards[j][i]<0){
                calcOddsFlag=false;
                break;
            }
        }
    }
    
    totalOpenCards=(totalSelectedCards-unSelectedCount);
    
    //If ateast 2 players are selected... enable the re-calc button.
    if(totalOpenCards==2*cardsPerPlayer && boardcount==0){
        self.buttonCalc.userInteractionEnabled=YES;
        self.buttonCalc.alpha=1;
    }
    
    //If all the players are selected, automatically calculate odds
    if( (totalOpenCards==playersPerGame*cardsPerPlayer||totalOpenCards>=(playersPerGame*cardsPerPlayer+3)) && calcOddsFlag)
    {
        [self performSelectorOnMainThread:@selector(clearStats) withObject:nil waitUntilDone:YES];
        [self performSelectorOnMainThread:@selector(spinnerStart) withObject:nil waitUntilDone:YES];
        [self performSelectorInBackground:@selector(addOdds) withObject:self];
    }
}

//FOR WAIT ANIMATION
- (void) spinnerStart{
    //[UIScreen mainScreen].brightness=screenBrightness-0.3;
    [backgroundView addSubview:spinner];
    
    backgroundView.userInteractionEnabled=NO;
    self.deckCollection.alpha=0.6;
    feltLabel.text=NSLocalizedString(@"Calculating...",nil);;
    [spinner startAnimating];
    //spinner.hidden=NO;
}

- (void) spinnerStop{
    //[UIScreen mainScreen].brightness=screenBrightness;
    feltLabel.font=[UIFont systemFontOfSize:12];
    feltLabel.text=[NSString stringWithFormat:NSLocalizedString(@"%d iterations", @"e.g. 10 iterations.  number of iterations we have computed."),iterationCount];
    self.deckCollection.alpha=1;
    if(totalOpenCards>=playersPerGame*cardsPerPlayer+5 && unSelectedCount==0){
        self.buttonCalc.userInteractionEnabled=NO;
        self.buttonCalc.alpha=0.6;
        self.deckCollection.userInteractionEnabled=NO;
        self.deckCollection.alpha=0.6;
    }
    else{
        self.buttonCalc.userInteractionEnabled=YES;
        self.buttonCalc.alpha=1;
    }
    backgroundView.userInteractionEnabled=YES;
    [spinner stopAnimating];
    [spinner removeFromSuperview];
}

//UNSELECT CARD FROM FELT AND push it back to the DECK
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    unsigned short i;
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    UIView *v = [backgroundView hitTest:location withEvent:nil];
    NSString *imageName;
    CardFromDeck *card = nil;
    imageName = [NSString stringWithFormat:@"%d.png",v.tag];
    //NSLog(@"handleSingleTap: objectAtIndex:%d",v.tag);
    
    @synchronized(cardImages){
        [cardImages replaceObjectAtIndex:v.tag withObject:imageName];
    }
    [card.cardInCell setImage:[UIImage imageNamed:imageName]];
    card.userInteractionEnabled=YES;
    
    @synchronized(self.deckCollection){
        //[self.deckCollection reloadItemsAtIndexPaths:[self deckCollection].indexPathsForVisibleItems];
        [self.deckCollection reloadData];
    }

    unSelected[unSelectedCount][0] = v.frame.origin.x;
    unSelected[unSelectedCount++][1] = v.frame.origin.y;
    
    if(v.frame.origin.y>190 && v.frame.origin.y<250){
        if(v.frame.origin.x==boardStartX) //Board
            boardCards[0]=-1;
        else if(v.frame.origin.x==boardStartX+45)
            boardCards[1]=-1;
        else if(v.frame.origin.x==boardStartX+90)
            boardCards[2]=-1;
        else if(v.frame.origin.x==boardStartX+135)
            boardCards[3]=-1;
        else if(v.frame.origin.x==boardStartX+180)
            boardCards[4]=-1;
    }
    else if(v.frame.origin.y<100) //Player 1&2
        i=0;
    else if(v.frame.origin.y>=330 && v.frame.origin.y<=420) //Player3&4
        i=2;
    {
        if(v.frame.origin.x==player1StartXPos)
            playerCards[i][0]=-1;
        else if(v.frame.origin.x==player1StartXPos+22)
            playerCards[i][1]=-1;
        else if(v.frame.origin.x==player1StartXPos+44)
            playerCards[i][2]=-1;
        else if(v.frame.origin.x==player1StartXPos+66)
            playerCards[i][3]=-1;
        else if(v.frame.origin.x==player2StartXPos)
            playerCards[i+1][0]=-1;
        else if(v.frame.origin.x==player2StartXPos+22)
            playerCards[i+1][1]=-1;
        else if(v.frame.origin.x==player2StartXPos+44)
            playerCards[i+1][2]=-1;
        else if(v.frame.origin.x==player2StartXPos+66)
            playerCards[i+1][3]=-1;
    }
    
    if(unSelectedCount){
        self.buttonCalc.alpha=1;
        self.buttonCalc.userInteractionEnabled=YES;
        self.deckCollection.userInteractionEnabled=YES;
        self.deckCollection.alpha=1;
        
        //Removing only the stat SUBViews (that has tag set to -2).
        NSArray *viewsToRemove2 = [backgroundView subviews];
        for (UIView *v in viewsToRemove2) {
            if(v.tag==-2 && v!=nil)
                [v removeFromSuperview];
        }
        feltLabel.font=[UIFont systemFontOfSize:17];
        feltLabel.text=gameTypeStr;
    }
    
    //NSLog(@"totalCards:%d unSelectedCount:%d",totalSelectedCards,unSelectedCount);
    
    //NSLog(@"tag:%d image:%@ x:%f Y:%f i:%d playercard[i]:%d player1StartXPos:%d player2StartXPos:%d ",v.tag,cardImages[v.tag],v.frame.origin.x, v.frame.origin.y,i,playerCards[i][0],player1StartXPos,player2StartXPos);

    if(unSelectedCount>=totalSelectedCards)
        [self reset:nil];
    
    if(v!=nil)
        [v removeFromSuperview];
}

#pragma mark - addOdds
-(void) addOdds{
    CardMask players[MAX_PLAYERS],common,dead;
    unsigned short i,j;
    int total=0,commonCount=0,playerCount=0,err;
    float evpct=0.0;
    enum_result_t result;
    
    /* DEBUG 
    for(j=0;j<playersPerGame;j++)
     for(i=0;i<cardsPerPlayer;i++)
        NSLog(@"Player 1 card is %d",playerCards[j][i]);
    */

    //BOARD Cards
    CardMask_RESET(common);
    if(boardcount){
        for(i=0;i<boardcount;i++)
            if(boardCards[i]>=0){
                CardMask_OR(common,common,StdDeck_cardMasksTable[boardCards[i]]);
                commonCount++;
                //NSLog(@"boardCard-%d: %d ; commonCount: %d",i,boardCards[i],commonCount);
            }
    }
    
    //Players Hand Cards
    for(j=0;j<playersPerGame;j++){
        CardMask_RESET(players[j]);
        if(playerCards[j][0]>=0){
            players[j]=StdDeck_cardMasksTable[playerCards[j][0]];
            playerCount++;
        }
    
        //The inner IF is to ensure, even if the FIRST CARD of a player is missing, we recalculate with remaining cards
        for(i=1;i<cardsPerPlayer;i++){
            if(playerCards[j][i]>=0){
                if(players[j].cards_n){
                    CardMask_OR(players[j],players[j],StdDeck_cardMasksTable[playerCards[j][i]]);
                }
                else{
                    players[j]=StdDeck_cardMasksTable[playerCards[j][i]];
                    playerCount++;
                }
            }
        }
    }
   
    CardMask_RESET(dead);
    CardMask_OR(dead, dead, common);
    for(j=0;j<playersPerGame;j++)
        CardMask_OR(dead, dead, players[j]);

    
//NSLog(@"cards:%s vs %s and dead:%s",StdDeck_maskString(players[0]),StdDeck_maskString(players[1]),StdDeck_maskString(dead));
/* HOLDEM ONly - Fast enumeration
 
    CardMask tmp1,tmp2,cards;
    HandVal h1,h2;
 
    CardMask_OR(players[0],players[0], common);
    CardMask_OR(players[1],players[1], common);
 
    CardMask_RESET(cards);
    ENUMERATE_N_CARDS_D(cards,5-commonCount,dead,
                      {
                          ++total;
                          CardMask_OR(tmp1,players[0],cards);
                          h1=Hand_EVAL_N(tmp1,7);
                          CardMask_OR(tmp2,players[1],cards);
                          h2=Hand_EVAL_N(tmp2,7);
                          
                          if(h1>h2) win1++;
                          else if(h1<h2) win2++;
                          else tie++;
                      });
 
    pct1=100*(float)(win1)/total;
    pct2=100*(float)(win2)/total;
    pcttie=100*(float)(tie)/total/2;
*/
    
    /* MIX game ENUMERATION */
    //game=game_omaha; //holdem, omaha, omaha8, 7stud, 7stud8, razz, lowball27
    
    enumResultClear(&result);
    if(commonCount<1 && game!=game_holdem)
        err = enumSample(game, players, common, dead, playerCount, commonCount,100000,orderMode, &result);
    else
        err = enumExhaustive(game, players, common, dead, playerCount, commonCount,orderMode, &result);
    
    if(err){
        NSLog(@"Library: Error Calculating Odds");
        [self performSelectorOnMainThread:@selector(reset:) withObject:(nil) waitUntilDone:YES];
    }
    else{
        total=result.nsamples;
    
        //NSLog(@"Printing results: Total:%d, h1:%d, h2:%d, tie:%d",total,win1,win2,tie);
    
        /* Displaying Stats Section */
        int statXpos=49,statYpos=60;
        if(isiPhone5)
            statYpos+=40;
        
        for(j=0;j<playerCount;j++){ /*Player 1-N*/

            //Start Odd# players at 49 and even at 200
            if(j%2==0){
                statXpos=49;
                if(j>1){ //Player3+
                    if(game==game_omaha8 || game==game_7stud8)
                        statYpos+=165;
                    else
                        statYpos+=190;
                }
            }
            else
                statXpos=200;
            
            evpct=(result.ev[j])*100/total;
            if(evpct>=(float)100/playerCount)
                [self addSubViewStat:statXpos :statYpos+52 :17 :[UIColor greenColor] :[NSString stringWithFormat:@"%.02f%%",evpct]];
            else
                [self addSubViewStat:statXpos :statYpos+52 :17 :[UIColor orangeColor] :[NSString stringWithFormat:@"%.02f%%",evpct]];
            evpct=0.0;
            
            if( game==game_omaha8 || (game==game_7stud8) ){
                if (commonCount<3){ //DISPLAY PERCENTAGE pre-flop.. monte-carlo
                    [self addSubViewStat:statXpos :statYpos+52+15 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"sc:%.02f%%",(float)result.nscoop[j]*100/total]];
                    [self addSubViewStat:statXpos :statYpos+52+30 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"hi:%.02f%%",(float)(result.nwinhi[j]+result.ntiehi[j])*100/total]];
                    [self addSubViewStat:statXpos :statYpos+52+45 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"lo:%.02f%%",(float)(result.nwinlo[j]+result.ntielo[j])*100/total]];
                }
                else{ //DISPLAY Actual Numbers pre-flop.. monte-carlo
                    [self addSubViewStat:statXpos :statYpos+52+15 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"sc:%d",result.nscoop[j]]];
                    [self addSubViewStat:statXpos :statYpos+52+30 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"hi:%d/%d",result.nwinhi[j],result.ntiehi[j]]];
                    [self addSubViewStat:statXpos :statYpos+52+45 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"lo:%d/%d",result.nwinlo[j],result.ntielo[j]]];
                }
            }
            else{
                [self addSubViewStat:statXpos :statYpos+52+15 :12 :[UIColor whiteColor] :[NSString stringWithFormat:@"%.02f/%.02f",(float)100*result.nwinhi[j]/total,100*(float)result.ntiehi[j]/total]];
            }
        }
    } /* End Of Displaying Stats Section */
    enumResultFree(&result);
    iterationCount=total;
    [self performSelectorOnMainThread:@selector(spinnerStop) withObject:(nil) waitUntilDone:NO];
}

// viewDidUnload is not called any more starting from iOS 6
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Not much we can do with low memory warning
}

- (void)setCardPos {
    int playerN;
    if(totalSelectedCards>(cardsPerPlayer*playersPerGame)){
        //Populating the Board Now.
        newCardX+=45;
    }
    else if(totalSelectedCards==(cardsPerPlayer*playersPerGame)){
        //Start Populating the Board Now.
        newCardX=boardStartX;
        newCardY=200;
        if(isiPhone5)
            newCardY+=40;
    }
    else if(totalSelectedCards%cardsPerPlayer==0){ //NEW Player
        playerN=(int)(totalSelectedCards/cardsPerPlayer)+1;
        if(playerN>2){ //plr 3 & 4, new Y needed
            newCardY=340;
            if(isiPhone5)
                newCardY+=40;
        }
        if(playerN%2) //Left side
            newCardX=player1StartXPos;
        else
            newCardX=player2StartXPos;
    }
    else{
        newCardX+=22;
        newCardY-=1;
    }
    //if(totalSelectedCards==(playersPerGame*cardsPerPlayer+3)) //3 is the #cards on flop
    //    newCardX+=1;
}

-(void) addSubViewImage:(int) x :(int) y :(NSString*) imageName :(int) index{
    int width=44;
    int height=50;
    CGRect location=CGRectMake(x, y, width,height);
    UIImage *cardImg = [UIImage imageNamed:imageName];
    
    addCard=[[UIImageView alloc] initWithFrame:location];
    [addCard setAutoresizingMask:UIViewAutoresizingNone];
    addCard.contentMode=UIViewContentModeTopLeft;
    addCard.clipsToBounds=YES;
    [addCard setImage:cardImg];
    addCard.userInteractionEnabled=YES;
    addCard.tag=index;
    singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [addCard addGestureRecognizer:singleFingerTap];
    [self performSelectorOnMainThread:@selector(addCardToFelt) withObject:(self) waitUntilDone:YES];
    bringFrontX=x;
    bringFrontY=y;
    [self performSelectorOnMainThread:@selector(bringFront) withObject:(self) waitUntilDone:YES];
}

//ANY UI change has to happen on main THREAD; so addSubview Part is delegated to MAIN Thread
//BEGIN: SELECTOR methods, to be used to force the thread main/background
-(void) addCardToFelt{
    [backgroundView addSubview:addCard];
}
-(void) bringFront{
    [self bringNextCardToFront:bringFrontX :bringFrontY];
}

-(void) addStatToFelt{
    [backgroundView addSubview:statLabel];
    [statLabel setNeedsDisplay];
}
//END: SELECTOR methods, to be used to force the thread main/background

-(void) addSubViewStat:(int) x :(int) y :(int) fontSize :(UIColor*) fontColor :(NSString*) statStr{
    CGRect plrstat=CGRectMake(x, y,100,20);
    statLabel = [[UILabel alloc] initWithFrame:plrstat];
    statLabel.backgroundColor=[UIColor clearColor];
    statLabel.textColor=fontColor;
    if(fontColor == [UIColor whiteColor] && game==game_omaha8)
        statLabel.font = [UIFont fontWithName:@"Courier" size:(float)fontSize];
    else
        statLabel.font = [UIFont systemFontOfSize:fontSize];
    statLabel.tag=-2;
    statLabel.text = statStr;
    [self performSelectorOnMainThread:@selector(addStatToFelt) withObject:(nil) waitUntilDone:YES];
    /* //ANOTHER WAY TO redirect a job to main thread
    dispatch_queue_t main_queue = dispatch_get_main_queue();
    dispatch_async(main_queue, ^{
        [self addStatToFelt];
        //dispatch_async(main_queue, ^{
        //    [self anothersubviewIFNEEDED];
        //});
    });
    */
}

-(void) bringNextCardToFront:(int) x :(int) y{
    UIView *vNext;
    do{
        vNext = [backgroundView hitTest:CGPointMake(x+23,y-1) withEvent:nil];
        [backgroundView bringSubviewToFront:vNext];
        x=vNext.frame.origin.x;
        y=vNext.frame.origin.y;
    }while(vNext.superview == backgroundView);
}

#pragma mark - reset
- (IBAction)reset:(id) sender {
    //NSLog(@"resetting..");
    NSString *imageName;
    NSArray *viewsToRemove = [backgroundView subviews];
    for (UIView *v in viewsToRemove) {
        if(v.tag>-1 && v!=nil){
            imageName=[NSString stringWithFormat:@"%d.png",v.tag];
            @synchronized(cardImages){
                [cardImages replaceObjectAtIndex:v.tag withObject:imageName];
            }
        }
        if(v.tag!=-1)
            [v removeFromSuperview];
    }
    [self initSetup];
    @synchronized(self.deckCollection){
        [self.deckCollection reloadData];
    }
    self.deckCollection.userInteractionEnabled=YES;
    self.deckCollection.alpha=1;
    // IF COMPLETE reload is needed
    //[self loadView];
    //[self viewDidLoad];
}

- (IBAction)revealMenu:(id)sender {
    if(!isMenuShown)
        [self showMenu];
    else
        [self hideMenu];
}

- (IBAction)calcOdds:(id)sender {
    
    bool reCalcFlag=FALSE;
    unsigned short i,j;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Pending Feature", nil) message:NSLocalizedString(@"Please select all the cards for each hand to recalculate.",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
    
    //IF each player has atleast one card, recalculate (initialize to false and set it true in the innerLoop), reverse if condition
    //IF all player cards are present (init to "true" in outer loop and set it false in innerloop), reverse if condition
    for(i=0;i<2;i++){   // 2-> atleast 2 players
        reCalcFlag=TRUE;
        for(j=0;j<cardsPerPlayer;j++){
            if(playerCards[i][j]<0){
                reCalcFlag=FALSE;
                break;
            }
        }
        if(!reCalcFlag)
            break;
    }
    
    //If player3+ hands are present and atleast one card missing, the flag is set to false.
    //If Player3+ hands are not present, it is fine. proceed with "TRUE".
    if(reCalcFlag)
        for(i=2;i<playersPerGame;i++){
            for(j=1;j<cardsPerPlayer;j++){
                if( (playerCards[i][j-1]<0 && playerCards[i][j]>=0) || (playerCards[i][j-1]>=0 && playerCards[i][j]<0) ){
                    reCalcFlag=FALSE;
                    break;
                }
                
            }
            if(!reCalcFlag)
                break;
        }

    //IF all the cards for each player are not present.. dont recalc.
    //if(reCalcFlag)
    //    if((totalSelectedCards-boardcount-unSelectedCount)%cardsPerPlayer!=0)
    //        reCalcFlag=FALSE;
    
    if(reCalcFlag){
        //Calling addOdds in the background - Spinner on the foreground.
        [self clearStats];
        [self spinnerStart];
        [self performSelectorInBackground:@selector(addOdds) withObject:self];
    }
    else{
        [alert show];
    }
}

#pragma mark - Gesture handler
-(void)handleSwipeLeft:(UISwipeGestureRecognizer*)recognizer{
    if(isMenuShown){
        [self hideMenu];
    }
}

-(void)handleSwipeRight:(UISwipeGestureRecognizer*)recognizer{
    //Right swipe to reveal Hidden Menu is interupting with card swipe.
    //So, Right Swipe will work only on the top 3/4 of the screen.
    CGPoint cp=[recognizer locationOfTouch:0 inView:backgroundView];
    if(self.view.frame.origin.x == 0 && cp.y<([UIScreen mainScreen].bounds.size.height*3/4))
        [self showMenu];
}

#pragma mark - animation

#define SOSideBarWidth 280

-(void)showMenu{
    isMenuShown = YES;
    //slide the content view to the right to reveal the menu
	[UIView animateWithDuration:.22
                     animations:^{
                         [backgroundView setFrame:CGRectMake(SOSideBarWidth, 0, self.view.frame.size.width, self.view.frame.size.height)];
                     }
     ];
}

-(void)hideMenu{
    //slide the content view to the left to hide the menu
    isMenuShown = NO;
	[UIView animateWithDuration:.22
                     animations:^{
                         [backgroundView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
                     }
     ];
}

-(void) clearStats{
    //Removing only the stat SUBViews (that has tag set to -2).
    NSArray *viewsToRemove3 = [backgroundView subviews];
    for (UIView *v in viewsToRemove3) {
        if(v.tag==-2 && v!=nil)
            [v removeFromSuperview];
    }
}

-(void) clearBoard:(BOOL) b1 andPlayer3:(BOOL) b2 currentPlayerCount:(int) plrCnt{
    NSArray *viewsToRemove3 = [backgroundView subviews];
    int yEnd=250;
    unsigned short i,j;
    CardFromDeck *card = nil;
    NSString *imageName;
    BOOL clearedFlag=NO;
    
    if(b2)
        yEnd=420;
    
    for (UIView *v in viewsToRemove3) {
        if(v!=nil && v.tag>=0 && v.frame.origin.y>=190 && v.frame.origin.y<=yEnd){
            if(plrCnt==3 && v.frame.origin.x<[UIScreen mainScreen].bounds.size.width/2 && v.frame.origin.y>[UIScreen mainScreen].bounds.size.height/2){
                continue; //if the player count selection changed from 3 to 4.. dont clear 3rd player cards
            }
            else{
                imageName = [NSString stringWithFormat:@"%d.png",v.tag];
                @synchronized(cardImages){
                    [cardImages replaceObjectAtIndex:v.tag withObject:imageName];
                }
                [card.cardInCell setImage:[UIImage imageNamed:imageName]];
                card.userInteractionEnabled=YES;
            
                [v removeFromSuperview];
                clearedFlag=YES;
            }
        }
    
    }
    
    //Only if any Player Card is Cleared, clear all the data of player cards except existing players
    if(clearedFlag){
        
        for(i=plrCnt;i<MAX_PLAYERS;i++) //player 0..1..plrCnt-1 cards are untouched
            for(j=0;j<MAX_CARDS_PER_PLAYER;j++)
                    playerCards[i][j]=-1;
    
        @synchronized(self.deckCollection){
            //[self.deckCollection reloadItemsAtIndexPaths:[self deckCollection].indexPathsForVisibleItems];
            [self.deckCollection reloadData];
        }
    
        self.deckCollection.alpha=1;
        self.deckCollection.userInteractionEnabled=YES;
        self.buttonCalc.alpha=1;
        self.buttonCalc.userInteractionEnabled=YES;
    }

    totalSelectedCards=playersPerGame*cardsPerPlayer;

    BOOL needUnselectAdjust=FALSE;
    int missingCardCount=0;
    
    //identify if any player cards are missing. If so, the unselected stack is needed to refil them.
    for(i=0;i<playersPerGame;i++){
        for(j=0;j<cardsPerPlayer;j++){
            if(playerCards[i][j]<0){
                totalSelectedCards--;
                needUnselectAdjust=TRUE;
                missingCardCount++;
            }
        }
    }
    
    //NSLog(@"1 total:%d playersPerGame:%d plrCnt:%d unselected:%d missingCardCount:%d",totalSelectedCards,playersPerGame,plrCnt,unSelectedCount,missingCardCount);
    //ONLY WHEN NEW SELECTION IS LESS THAN PREVIOUS and needs unselected STACK adjustment - enter this
    if( playersPerGame<=plrCnt && needUnselectAdjust){
        //unselected stack has invalid placeholders, trash everything.
        if(unSelectedCount>missingCardCount)
            [self reset:nil];
        //If none of the subviews (players cards) were cleared and remaining cards are <= the needed, trash the unselected data.
        else if(totalSelectedCards==plrCnt*cardsPerPlayer && !clearedFlag)
            unSelectedCount=0;
        //IF the player cards are incomplete and unselected stack contains data, adjust total selected accordingly.
        else if(unSelectedCount && (totalSelectedCards-boardcount)<plrCnt*cardsPerPlayer)
            totalSelectedCards+=(unSelectedCount-boardcount);
    }
    else
        unSelectedCount=0;

    //NSLog(@"2 total:%d playersPerGame:%d unselected:%d missingCardCount:%d",totalSelectedCards,playersPerGame,unSelectedCount,missingCardCount);
    
    //CLEAR THE BOARD - IRRESPECTIVELY
    for(i=0;i<boardcount;i++)
        boardCards[i]=-1;
    boardcount=0;
    iterationCount=0;
}

@end //END OF VIEW CONTROLLER