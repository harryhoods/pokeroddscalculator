/* Public declarations for combinations.c */

#ifndef COMBINATIONS_H
#define COMBINATIONS_H

#include "pokereval_export.h"

typedef void *Combinations;

extern POKEREVAL_EXPORT void free_combinations(Combinations c);
extern POKEREVAL_EXPORT Combinations init_combinations(int nuniv, int nelem);
extern POKEREVAL_EXPORT int num_combinations(Combinations c);
extern POKEREVAL_EXPORT void get_combination(Combinations c, int cnum, int *elems);

#endif
