

#ifndef LIBPOKEREVAL_EXPORT
#define LIBPOKEREVAL_EXPORT 1


#if defined(_MSC_VER)
    #pragma warning( disable : 4244 )
    #pragma warning( disable : 4251 )
    #pragma warning( disable : 4267 )
    #pragma warning( disable : 4275 )
    #pragma warning( disable : 4290 )
    #pragma warning( disable : 4786 )
    #pragma warning( disable : 4305 )
#endif

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)

	#ifndef POKERVAL_DLL
		#ifdef __cplusplus
			#define POKEREVAL_EXPORT "C"
		#else
			#define POKEREVAL_EXPORT
		#endif
	#else
		#ifdef POKEREVAL_LIBRARY
			#ifdef __cplusplus
				#define POKEREVAL_EXPORT "C" __declspec(dllexport)
			#else
				#define POKEREVAL_EXPORT "C"
			#endif
		#else
			#ifdef __cplusplus
				#define POKEREVAL_EXPORT "C" __declspec(dllimport)
			#else
				#define POKEREVAL_EXPORT "C"
			#endif
		#endif
	#endif

#else
	#ifdef __cplusplus
		#define POKEREVAL_EXPORT "C"
	#else
		#define POKEREVAL_EXPORT
	#endif
#endif

#endif
