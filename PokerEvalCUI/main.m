//
//  main.m
//  PokerEvalCUI
//
//  Created by Hari Prabhakaran on 3/3/13.
//  Copyright (c) 2013 Hari Prabhakaran. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
