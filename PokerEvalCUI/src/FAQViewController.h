//
//  FAQViewController.h
//  ScoopOdds
//
//  Created by Chang Luo on 3/20/13.
//  Copyright (c) 2013 Hari Prabhakaran & Chang Luo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQViewController : UIViewController
-(IBAction) onDone;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@end
